# Yelloscope #

My fork of [Syscomp Design's](https://www.syscompdesign.com/) software for their USB Oscilloscopes.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Yelloscope](#yelloscope)
    - [Firmware hex files](#firmware-hex-files)
    - [Sample waveforms for the arbitrary waveform generator](#sample-waveforms-for-the-arbitrary-waveform-generator)
    - [Change log](#change-log)
        - [Release version 1.0.0 for Linux](#release-version-100-for-linux)

<!-- markdown-toc end -->


## Firmware hex files ##

Each supported device (CGM-101, CGR-201, and SIG-101) contains a
microcontroller and an FPGA.  Firmware for the individual
microcontrollers is in `src/Firmware`.

## Sample waveforms for the arbitrary waveform generator ##

Look for waveform examples in `doc/example_waveforms`.

## Change log ##

### Release version 1.0.0 for Linux ###

This is my first attempt at a
[Starpack](https://wiki.tcl-lang.org/page/Starpack) for the Yelloscope
software.  The top-level makefile needs to be rewritten to build for
other platforms.

* Window and infrastructure for a console log window.  Logged messages
  get sent to the window and to a log file in a `log` directory.
