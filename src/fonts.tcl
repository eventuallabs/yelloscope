# fonts for the console logger.
font create FixedFont -family {DejaVu Sans Mono} -size 12

# Font for the terminal
font create terminal_font -family {DejaVu Sans Mono} -size 12

# Font for names of things
font create NameFont -family {Arial} -size 10 -weight bold

# Font for console log
font create log_font -family {DejaVu Sans Mono} -size 10

# Big font for test results
font create ShoutFont -family TkFixedFont -size 30

# Font for counters
font create counter_font -family TkFixedFont -size 10

# Font for column headers
font create header_font -family Arial -size 12 -weight bold

# Font for frame labels
font create frame_label_font -family Arial -size 12

# Fonts for short operator entry
font create big_entry_font -family {Arial} -size 20

# Fonts for big fixed-width
font create big_fixed_font -family {DejaVu Sans Mono} -size 20
