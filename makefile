

# The name of the program or executable -- no extensions
progname = yelloscope

# The main tcl file.  I sometimes just call this main, and sometimes
# something else.  No extension.
main_tcl_file = main

# The version code.  This is where the code should be set.  This value
# will be written to the source files and show up in the filename.
# I'll use semantic versioning, so that each version should have
# major, minor, and patch codes.
revcode := 1.0.0

# What platform are we making starpacks for?
#
# Can be: windows or linux
starpack_platform = linux

# Path to the TCL files making up the source
source_directory = src

# Directory for icons under the source path
#
# The Syscomp Design source code has both image and icon directories.
# icon_directory = Icons
icon_directories = Icons
icon_directories += Icons/Signature
icon_directories += Icons/Scope
icon_directories += Icons/NetworkAnalyzer

# Directory for images under the source path
#
# The Syscomp Design source code has both image and icon directories.
image_directories = Images

# Path for tcl modules to be copied into the starkit.
ifeq ($(starpack_platform),linux)
# Platform is Linux
#
# Install tcllib from the Ubuntu package manager
tcllib_path = /usr/share/tcltk/tcllib1.20

# Install tklib from the Ubuntu package manager
tklib_path = /usr/share/tcltk/tklib0.7
endif



# Local module path
#
# This is used for modules outside of tcllib.  For example tzint needs
# to be built manually.
local_module_path = modules

# Test files directory
#
# This directory and all its contents will be copied directly into the
# starkit.  Test datafiles must have the suffix .testdata for
# portaterm to find them.
test_files_directory = test_files

# List of modules to copy over into the starkit.
#
# I've been getting image modules from
# /opt/ActiveTcl-8.6/lib/teapot/package/linux-glibc2.3-x86_64/lib
external_packages = $(tcllib_path)/log \
                    $(tklib_path)/tooltip \
                    $(tklib_path)/canvas

external_packages += /usr/lib/tcltk/sqlite3
external_packages += /usr/lib/tcltk/x86_64-linux-gnu/Img1.4.11
external_packages += /usr/share/tcltk/bwidget1.9.13
external_packages += /usr/lib/tcltk/x86_64-linux-gnu/Tktable2.10
external_packages += $(tcllib_path)/math



# The tclkits to package in the starpacks.
#
# Make sure to get a Tclkit with Tk -- so you'll need a Tclkit with
# Exensions.
#
# Minimal tclkits contain:
#   1. Tcl (KitCreator always includes this)
#   2. IncrTcl (Check box in KitCreator)
#   3. TclVFS (KitCreator always includes this)
#   4. Metakit (Check box in KitCreator)
#
# This application requires the extra packages:
#   1. Tk (Check box in KitCreator)
#
# Download tclkits from:
#   http://tclkits.rkeene.org/fossil/wiki/Downloads
#   or...
#   https://sourceforge.net/projects/twapi/
#
# Stock tclkits have their own icons.  If you want a custom set for
# Windows, you can download the Resource Hacker software.  This allows
# replacing the icons with a *.ico file containing a range of
# resolutions.  The icofx software is useful for creating this
# multi-image file.
#
# Note that windows keeps an icon cache.  To clear it, call
#
# ie4uinit.exe -ClearIconCache
#
# ...and your icons should show up.
# win64_kit = "wraptools/tclkit-8.6.4-win64-ix86.exe"
win64_kit = wrap/tclkit-gui-8_6_10-twapi-4_3_8-x64-max.exe

# Create 64-bit linux tclkit with KitCreator -- downloaded from Roy Keene.
lin64_kit = wrap/kc_8_6_11_linux64_tclkit

# The tcl shell needed to execute the Starkit Developer tools.
tclsh = tclsh

# Download Starkit Developer Extension from http://equi4.com/pub/sk/
sdx = "wrap/sdx.kit"

inkscape = "d:/programs/inkscape/bin/inkscape.exe"

#------------------------- Done with configuration ---------------------

source_icon_directories = $(addprefix src/, $(icon_directories))
source_image_directories = $(addprefix src/, $(image_directories))

# This list will be generated based on the svg files in the src/icons
# directory.  PNG files will automatically be created from svg by Inkscape.
icon_png_files = $(addsuffix .png, $(basename $(wildcard $(addsuffix *.svg, $(source_directory)/$(icon_directory)))))

source_tcl_files := $(wildcard $(source_directory)/*.tcl)

source_icon_files = $(foreach dir, $(source_icon_directories), $(wildcard $(dir)/*.png))
source_icon_files += $(foreach dir, $(source_icon_directories), $(wildcard $(dir)/*.gif))

source_image_files = $(foreach dir, $(source_image_directories), $(wildcard $(dir)/*.png))
source_image_files += $(foreach dir, $(source_image_directories), $(wildcard $(dir)/*.gif))

# Source files will be relocated in the starkit virtual file system
vfs_icon_files = $(subst src, $(progname).vfs/lib/app-$(progname), $(source_icon_files))
vfs_image_files = $(subst src, $(progname).vfs/lib/app-$(progname), $(source_image_files))

vfs_tcl_files = $(addprefix $(progname).vfs/lib/app-$(progname)/, $(notdir $(source_tcl_files)))

starkit_icon_files = $(addprefix $(progname).vfs/lib/app-$(progname)/$(icon_directory), $(notdir $(source_icon_files)))

copied_packages = $(addprefix $(progname).vfs/lib/, $(notdir $(external_packages)))


usage_text_width = 20
indent_text_width = 7
target_text_width = 15

# All these printf statements look confusing, but the idea here is to
# separate the layout from the content.  Always use the same printf
# line with a continuation character at the end.  Put your content on
# the next line.

help:
	@echo "Makefile for $(progname)"
	@echo ""
	@echo "Usage:"
	@printf "%$(indent_text_width)s %-$(target_text_width)s %s\n" \
          "make" "starkit" \
          "Make a starkit for $(starpack_platform)"
	@echo '   make testrun                                       '
	@echo '       Run the starkit in a temporary directory (testrun)       '
	@echo '   make win64                                         '
	@echo '       Make win64                                     '
	@echo '   make icon_pngs'
	@echo '       Create png icons from svg'
	@echo '   make debug_make                                         '
	@echo '------------------------------------------------------'
	@echo '   make clean                                         '
	@echo '       Clean up temporary files                       '

.PHONY: debug_make
debug_make:
	@echo "vfs files: $(vfs_files)"
	@echo ""
	@echo "Source tcl files: $(source_tcl_files)"
	@echo ""
	@echo "Source icon files: $(source_icon_files)"
	@echo ""
	@echo "Source image files: $(source_image_files)"
	@echo ""
	@echo "VFS icon files: $(vfs_icon_files)"
	@echo ""
	@echo "External packages: $(external_packages)"
	@echo ""
	@echo "Copied packages: $(copied_packages)"

.PHONY: icon_pngs
icon_pngs: $(icon_png_files)

.PHONY: testrun
testrun: $(progname).kit
	mkdir -p testrun
	cp $< testrun
	cd testrun; wish $(progname).kit &

# Make the starkit.
.PHONY: starkit
starkit: $(progname).kit
$(progname).kit: $(source_tcl_files) \
                 $(progname).vfs/main.tcl \
                 $(vfs_tcl_files) \
                 $(vfs_icon_files) \
                 $(vfs_image_files) \
                 $(copied_packages)
	@echo 'Making starkit'
	$(tclsh) $(sdx) wrap $(progname)
	mv $(progname) $@

.gitignore:
	echo 'starpacks' > $@
	echo '*~' >> $@

# Now source all the tcl code into the top of the vfs tree.  Note that
# you also have to replace each 'source' line from your tcl files with
# a command to source a file relative to the top of the vfs.
# Otherwise, tcl has no idea where these files are.
$(progname).vfs/main.tcl:
	mkdir -p $(dir $@)
	echo 'package require starkit' > $@
	echo 'if {[starkit::startup] ne "sourced"} {' >> $@
	echo '    source [file join $$starkit::topdir'\
             'lib/app-$(progname)/$(main_tcl_file).tcl]' >> $@
	echo '}' >> $@

# Creating the lib directory also copies all the needed modules.
$(progname).vfs/lib: $(progname).vfs
	mkdir -p $@
	cp -R $(external_modules) $@
	chmod -R a+rx *

$(progname).vfs/lib/%: $(tcllib_path)/%
	cp -R $< $@

$(progname).vfs/lib/%: $(tklib_path)/%
	cp -R $< $@

$(progname).vfs/lib/%: /usr/lib/tcltk/%
	cp -R $< $@

$(progname).vfs/lib/%: /usr/lib/tcltk/x86_64-linux-gnu/%
	cp -R $< $@

$(progname).vfs/lib/%: /usr/share/tcltk/%
	cp -R $< $@

# Create the icons directory to mirror that used in development.
$(progname).vfs/lib/app-$(progname)/$(icon_directory):
	mkdir -p $@
	cp $(source_icon_files) $@

# Fix file path strings for starkits:
# Was:
#   source something.tcl
# Becomes:
#   source [file join $starkit::topdir lib/app-bitdecode/something.tcl]
#
# Was:
#   set wmiconfile icons/calc_16x16.png
# Becomes:
#   set wmiconfile [file join $starkit::topdir lib/app-bitdecode/icons/calc_16x16.png]
starkit_joinpath := [file join $$starkit::topdir lib/app-$(progname)/&]

# Copy source files into the starkit, filtering them to create proper
# location references inside the virtual file system.
#
# 1. Create proper path references for tcl source files.
# 2. Create proper path references for png icon files.
# 3. Create proper path references for tcl module (.tm) files
# 4. Create proper path references for test log (.testdata) files
# 5. Set the revision code to the setting in this makefile
$(progname).vfs/lib/app-$(progname)/%: $(source_directory)/%
	mkdir -p $(dir $@)
	sed 's,[[:graph:]]*\.tcl,$(starkit_joinpath),g'< $< | \
	  sed 's,[[:graph:]]*\.png,$(starkit_joinpath),g' | \
          sed 's,[[:graph:]]*\.gif,$(starkit_joinpath),g' | \
          sed 's,[[:graph:]]*\.tm,$(starkit_joinpath),g' | \
          sed 's,[[:graph:]]*\.testdata,$(starkit_joinpath),g' | \
          sed 's/set revcode.*/set revcode $(revcode)/g' > $@

# The starpack is the same as a starkit with a built-in tclkit.  The
# wrap command is the only difference.  Naming convention for these
# starpacks comes from tclkits at:
# http://tclkits.rkeene.org/fossil/wiki/Downloads
.PHONY: win64
win64: starpacks/$(progname)-$(revcode)-win64-ix86.exe
starpacks/$(progname)-$(revcode)-win64-ix86.exe: starkit
	mkdir -p $(dir $@)
	$(tclsh) $(sdx) wrap $(progname) -runtime $(win64_kit)
	mv $(progname) $@

lin64: starpacks/$(progname)-$(revcode)-linux-x86_64
starpacks/$(progname)-$(revcode)-linux-x86_64: starkit
	mkdir -p $(dir $@)
	$(tclsh) $(sdx) wrap $(progname) -runtime $(lin64_kit)
	mv $(progname) $@

# Make icon files from svg using Inkscape.  A good size for icons is
# 22x22
src/icons/%.png: src/icons/%.svg
	$(inkscape) --export-filename=$@ --export-width 22 $<

.PHONY: clean
clean:
	rm -rf $(progname).vfs
	rm -f $(progname).bat
	rm -f *.log
	rm -f $(progname).cfg
	rm -f $(progname).kit
	rm -rf testrun

